package cn.edu.cqvie.service.impl;

import cn.edu.cqvie.Constants;
import cn.edu.cqvie.dao.MessageContactRepository;
import cn.edu.cqvie.dao.MessageContentRepository;
import cn.edu.cqvie.dao.MessageRelationRepository;
import cn.edu.cqvie.dao.UserRepository;
import cn.edu.cqvie.entity.*;
import cn.edu.cqvie.service.MessageService;
import cn.edu.cqvie.vo.MessageContactVO;
import cn.edu.cqvie.vo.MessageVO;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageContentRepository contentRepository;
    @Autowired
    private MessageRelationRepository relationRepository;
    @Autowired
    private MessageContactRepository contactRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public MessageVO sendNewMsg(long senderUid, long recipientUid, String content, int msgType) {
        Date currentTime = new Date();
        /**存内容*/
        MessageContent messageContent = new MessageContent();
        messageContent.setSenderId(senderUid);
        messageContent.setRecipientId(recipientUid);
        messageContent.setContent(content);
        messageContent.setMsgType(msgType);
        messageContent.setCreateTime(currentTime);
        messageContent = contentRepository.saveAndFlush(messageContent);
        Long mid = messageContent.getMid();

        /**存发件人的发件箱*/
        MessageRelation messageRelationSender = new MessageRelation();
        messageRelationSender.setMid(mid);
        messageRelationSender.setOwnerUid(senderUid);
        messageRelationSender.setOtherUid(recipientUid);
        messageRelationSender.setType(0);
        messageRelationSender.setCreateTime(currentTime);
        relationRepository.save(messageRelationSender);

        /**存收件人的收件箱*/
        MessageRelation messageRelationRecipient = new MessageRelation();
        messageRelationRecipient.setMid(mid);
        messageRelationRecipient.setOwnerUid(recipientUid);
        messageRelationRecipient.setOtherUid(senderUid);
        messageRelationRecipient.setType(1);
        messageRelationRecipient.setCreateTime(currentTime);
        relationRepository.save(messageRelationRecipient);

        /**更新发件人的最近联系人 */
        Optional<MessageContact> messageContactSender = contactRepository.findById(
                new ContactMultiKeys(senderUid, recipientUid));
        MessageContact messageContact = new MessageContact();
        if (messageContactSender.isPresent()) {
            messageContact = messageContactSender.get();
            messageContact.setMid(mid);
        } else {
            messageContact = new MessageContact();
            messageContact.setOwnerUid(senderUid);
            messageContact.setOtherUid(recipientUid);
            messageContact.setMid(mid);
            messageContact.setCreateTime(currentTime);
            messageContact.setType(0);
        }
        contactRepository.save(messageContact);

        // 更新收件人的最近联系人
        Optional<MessageContact> messageContactRecipient =
                contactRepository.findById(new ContactMultiKeys(recipientUid, senderUid));
        MessageContact recipient = new MessageContact();
        if (messageContactRecipient.isPresent()) {
            recipient = messageContactRecipient.get();
            recipient.setMid(mid);
        } else {
            recipient = new MessageContact();
            recipient.setOwnerUid(recipientUid);
            recipient.setOtherUid(senderUid);
            recipient.setMid(mid);
            recipient.setCreateTime(currentTime);
            recipient.setType(1);
        }
        contactRepository.save(recipient);

        /**更未读更新 */
        redisTemplate.opsForValue().increment(recipientUid + "_T", 1); //加总未读
        redisTemplate.opsForHash().increment(recipientUid + "_C", senderUid, 1); //加会话未读

        /** 待推送消息发布到redis */
        User self = userRepository.findById(senderUid).get();
        User other = userRepository.findById(recipientUid).get();
        MessageVO messageVO = new MessageVO(mid, content, self.getUid(),
                messageContact.getType(), other.getUid(),
                messageContent.getCreateTime(), self.getAvatar(),
                other.getAvatar(), self.getUsername(), other.getUsername());
        redisTemplate.convertAndSend(Constants.WEBSOCKET_MSG_TOPIC,
                JSONObject.toJSONString(messageVO));

        return messageVO;
    }

    @Override
    public List<MessageVO> queryConversationMsg(long ownerUid, long otherUid) {
        List<MessageRelation> relationList =
                relationRepository.findAllByOwnerUidAndOtherUidOrderByMidAsc(ownerUid, otherUid);
        return composeMessageVO(relationList, ownerUid, otherUid);
    }

    @Override
    public List<MessageVO> queryNewerMsgFrom(long ownerUid, long otherUid, long fromMid) {
        List<MessageRelation> relationList =
                relationRepository.findAllByOwnerUidAndOtherUidAndMidIsGreaterThanOrderByMidAsc(
                        ownerUid, otherUid, fromMid);
        return composeMessageVO(relationList, ownerUid, otherUid);
    }

    private List<MessageVO> composeMessageVO(List<MessageRelation> relationList,
                                             long ownerUid, long otherUid) {
        if (null != relationList && !relationList.isEmpty()) {
            /** 先拼接消息索引和内容 */
            List<MessageVO> msgList = Lists.newArrayList();
            User self = userRepository.findById(ownerUid).get();
            User other = userRepository.findById(otherUid).get();
            relationList.stream().forEach(relation -> {
                Long mid = relation.getMid();
                Optional<MessageContent> contentVO = contentRepository.findById(mid);
                if (contentVO.isPresent()) {
                    String content = contentVO.get().getContent();
                    MessageVO messageVO = new MessageVO(mid, content,
                            relation.getOwnerUid(), relation.getType(),
                            relation.getOtherUid(), relation.getCreateTime(),
                            self.getAvatar(), other.getAvatar(), self.getUsername(),
                            other.getUsername());
                    msgList.add(messageVO);
                }
            });

            /** 再变更未读 */
            Object convUnreadObj = redisTemplate.opsForHash().get(
                    ownerUid + Constants.CONVERSION_UNREAD_SUFFIX, otherUid);
            if (null != convUnreadObj) {
                long convUnread = Long.parseLong((String) convUnreadObj);
                redisTemplate.opsForHash().delete(ownerUid + Constants.CONVERSION_UNREAD_SUFFIX, otherUid);
                long afterCleanUnread = redisTemplate.opsForValue().increment(ownerUid + Constants.TOTAL_UNREAD_SUFFIX, -convUnread);
                /** 修正总未读 */
                if (afterCleanUnread <= 0) {
                    redisTemplate.delete(ownerUid + Constants.TOTAL_UNREAD_SUFFIX);
                }
            }
            return msgList;
        }
        return null;
    }

    @Override
    public MessageContactVO queryContacts(long ownerUid) {
        List<MessageContact> contacts = contactRepository.findMessageContactsByOwnerUidOrderByMidDesc(
                ownerUid);
        if (contacts != null) {
            User user = userRepository.findById(ownerUid).get();
            long totalUnread = 0;
            Object totalUnreadObj = redisTemplate.opsForValue().get(
                    user.getUid() + Constants.TOTAL_UNREAD_SUFFIX);
            if (null != totalUnreadObj) {
                totalUnread = Long.parseLong((String) totalUnreadObj);
            }

            MessageContactVO contactVO = new MessageContactVO(user.getUid(),
                    user.getUsername(), user.getAvatar(), totalUnread);
            contacts.stream().forEach(contact -> {
                Long mid = contact.getMid();
                Optional<MessageContent> contentVO = contentRepository.findById(mid);
                User otherUser = userRepository.findById(contact.getOtherUid()).get();

                if (contentVO.isPresent()) {
                    long convUnread = 0;
                    Object convUnreadObj = redisTemplate.opsForHash().get(
                            user.getUid() + Constants.CONVERSION_UNREAD_SUFFIX, otherUser.getUid());
                    if (null != convUnreadObj) {
                        convUnread = Long.parseLong((String) convUnreadObj);
                    }
                    MessageContactVO.ContactInfo contactInfo =
                            contactVO.new ContactInfo(otherUser.getUid(),
                                    otherUser.getUsername(), otherUser.getAvatar(), mid,
                                    contact.getType(),
                                    contentVO.get().getContent(),
                                    convUnread, contact.getCreateTime());
                    contactVO.appendContact(contactInfo);
                }
            });
            return contactVO;
        }
        return null;
    }

    @Override
    public long queryTotalUnread(long ownerUid) {
        long totalUnread = 0;
        Object totalUnreadObj = redisTemplate.opsForValue().get(
                ownerUid + Constants.TOTAL_UNREAD_SUFFIX);
        if (null != totalUnreadObj) {
            totalUnread = Long.parseLong((String) totalUnreadObj);
        }
        return totalUnread;
    }
}
